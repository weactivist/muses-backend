'use strict';

var mongoose = require('mongoose');
var Article = mongoose.model('Article');
var util = require('../util');

exports.index = function(req, res) {
    Article.findOne({_id: req.params.id}, function(error, data) {
        util.jsonResponse(res, error, data);
    });
};

exports.list = function(req, res) {
    Article.find({}, function(error, data) {
        util.jsonResponse(res, error, data);
    });
};
