'use strict';

module.exports = function(app) {
    let articles = require('./controllers/articles.js');
    let util = require('./util.js');

    app.route('/')
        .get(function(req, res) {
            var routes = [];

            for( var i = 0; i < app._router.stack.length; i++ ) {
                var stack = app._router.stack[i];

                if( stack.route !== undefined ) {
                    routes.push( {
                        path: stack.route.path,
                        methods: stack.route.methods
                    } );
                }
            }

            res.json({
                endpoints: routes
            });
        });
    app.route('/articles')
        .get(articles.list);
    app.route('/articles/:id')
        .get(articles.index);
    app.route('*')
        .all( function(req, res) {
            res.send('404: Page not found', 404);
        } );
};
