'use strict';

exports.authenticated = function(req, res, next) {
    if(req.user) {
        next();
    }
    else {
        res.redirect('/');
    }
};

exports.htmlResponse = function(template, req, res, err, data) {
    res.set('Content-Type', 'text/html');

    if(err) {
        res.status(400).render('error', {message: err});
    }
    else if(!data) {
        res.status(404).render('error', {message: '404: Not Found'});
    }
    else {
        data.user = req.user;
        res.status(200).render(template, data);
    }
};

exports.jsonResponse = function(res, err, data) {
    if(err) {
        res.status(400).json(err);
    }
    else if(!data) {
        res.status(404).json({ error: '404: Not Found' });
    }
    else {
        res.status(200).json(data);
    }
};
