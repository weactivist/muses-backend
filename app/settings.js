'use strict';

let InstantArticleFeed = require('./schema/InstantArticleFeed');

let settings = {
    feeds: [
        {
            title: 'Auto Motor & Sport',
            link: 'http://www.mestmotor.se/automotorsport/facebook/feed/articles'
        },
        {
            title: 'King Magazine',
            link: 'http://www.kingmagazine.se/facebook/feed/articles'
        },
        {
            title: 'Manolo',
            link: 'http://www.manolo.se/facebook/feed/articles'
        },
        {
            title: 'Svensk Golf',
            link: 'http://www.svenskgolf.se/facebook/feed/articles'
        },
        {
            title: 'Recharge',
            link: 'http://www.mestmotor.se/recharge/facebook/feed/articles'
        },
        {
            title: 'Vagabond',
            link: 'http://www.vagabond.se/facebook/feed/articles'
        },
        {
            title: 'Example',
            link: 'https://raw.githubusercontent.com/jkiss/Instant-Article-Dev-Help-Doc/master/Instant-Article-RSS-example.rss'
        }
    ],
    provision: function(callback) {
        Promise.all(settings.feeds.map(function(feed) {
            return new Promise(function(resolve, reject) {
                InstantArticleFeed.findOne({link: feed.link}).exec().then(function(item) {
                    if(!item) {
                        let item = new InstantArticleFeed();

                        item.title = feed.title;
                        item.link = feed.link;

                        item.save(function(error) {
                            if(error) {
                                reject(error);
                            }
                            else {
                                resolve(item);
                            }
                        });
                    }
                    else {
                        resolve(item);
                    }
                }).catch(function(error) {
                    reject(error);
                });
            });
        })).then(function(items) {
            if(typeof callback === 'function') {
                callback(null, items);
            }
        }).catch(function(error) {
            if(typeof callback === 'function') {
                callback(error, []);
            }
        });
    }
};

module.exports = settings;
