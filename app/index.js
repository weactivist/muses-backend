'use strict';

let express = require('express');
let mongoose = require('mongoose');
let Article = require('./schema/Article');
let settings = require('./settings');
let routes = require('./routes');

// Connect to DB
mongoose.connect(process.env.MONGODB_URI).catch(function(error) {
    console.log('Connection error: ', error);
});

// Provision on startup
settings.provision(function(error, items) {
    if(error) {
        console.log('ERROR: ', error);
    }

    console.log(items);
});

let app = express();

routes(app);

app.listen(process.env.PORT, function(){
  console.log('App listening on port ' + process.env.PORT);
});
